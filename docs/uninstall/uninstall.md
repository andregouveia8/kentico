<a name="removethepagetemplate"></a>
### 1# Remove the page template

**1# CHANGE THE PAGE TEMPLATE** <small>(only if you are using one of the theme's page templates)</small>

If you're using any of the theme's page templates on your website, you will need to change it to any of the other ones available before removing the page template and the theme's files. 

1. Open the **Applications list**;
2. Click on **Content Management** and then **Pages**;

	![kentico9_rtd_installation-25.png](https://bitbucket.org/repo/yG87gj/images/2753728020-kentico9_rtd_installation-25.png)

3. With your site selected on the pages list, click on **Properties** and then on **Template**;
4. With the option **Use own page template** selected, click on **Select**;
5. On the templates list, choose any of the page templates avaialble but one of the theme's page template. Ex: "Blank Master Page". Click on the page template and then click on **Select**;
	
	![kentico_rtd_uninstall-1.png](https://bitbucket.org/repo/yG87gj/images/3694699499-kentico_rtd_uninstall-1.png)
	 
6. Last but not least, click on **Save**;


**2# Delete the page template**

Now that you have changed the page template, you can go ahead and delete it. 

1. Open the **Applications list**;
2. Click on **Development** and then on **Page Templates**;
 	
 	![kentico9_rtd_installation-11.png](https://bitbucket.org/repo/yG87gj/images/3137385635-kentico9_rtd_installation-11.png)
 	
3. On the templates list, click to select the theme's page template;
4. Now click on the trash icon to delete the page template;

	![kentico_rtd_uninstall-3.png](https://bitbucket.org/repo/yG87gj/images/1906120574-kentico_rtd_uninstall-3.png)
	
5. Click **Save**;

Page templates removed! ✅

-----
<a name="deletethebindmenu"></a>
### 2# Delete the BindMENU 

![BindMENU.gif](https://bitbucket.org/repo/yG87gj/images/1008315636-BindMENU.gif)

1. Go back to your **Applications dashboard**;
2. Click on **Development**, and then **CSS stylesheets**;
2. On the list, look for all the BindMENU's you have previously installed and for each one click on the trash icon;

BindMENU deleted! ✅

----------

<a name="removethethemecssstylesheet"></a>
### 3# Remove the theme's CSS stylesheet

**1# CHANGE THE SITE CSS STYLESHEET** <small>(only if you are using the theme on any of your website page)</small>

1. Open the **Applications list**;
2. Click on **Configuration** and then click on **Sites**;

	 ![kentico9_rtd_installation-7.png](https://bitbucket.org/repo/yG87gj/images/2958331868-kentico9_rtd_installation-7.png)
	 
3. On the website from where you want to remove the CSS stylesheet, click on the **Edit** icon;
4. On the **General** tab, under the **Style sheets** section, in the **Site CSS stylesheet**, open the dropdown menu and select another CSS stylesheet;
5. Click **Save**.


**2# DELETE THE THEME'S CSS STYLESHEET**

1. Open the **Applications list**;
2. Click on **Development** and then click on **CSS stylesheets**;

	 ![kentico9_rtd_installation-7.png](https://bitbucket.org/repo/yG87gj/images/2958331868-kentico9_rtd_installation-7.png)	
	
3. Look for the theme's CSS stylesheets and click on the trash icon;

Theme removed! ✅
