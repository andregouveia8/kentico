<a name="addthecssstylesheets"></a>
### 1# Add the CSS Stylesheets 

**1# UPLOAD THE CSS STYLESHEETS**

1. Log in to your Kentico website as an Admin;
2. On your **Applications dashboard**, click on **CSS stylesheets**;  

	![kentico9_rtd_installation-2.png](https://bitbucket.org/repo/yG87gj/images/1934054468-kentico9_rtd_installation-2.png)
	
3. Click on **New CSS stylesheet**
4. Add the template names:

	![kentico9_rtd_installation-4.png](https://bitbucket.org/repo/yG87gj/images/177115868-kentico9_rtd_installation-4.png)
	
	- On **Display name**, add your theme name.
	
	- On **Code name**, add your theme name.
	
	- On **Code**, paste the code from the **yourthemename.uncompressed.css** file. You can find this file by opening your theme's package.  

5. You can leave the option **"assign to website..."** checked;
6. Click **Save**.  


	<p class="alert-warning">Since Kentico loads all the CSS files from your theme's package even after adding your theme's CSS stylesheets to your website, to prevent any conflicts, you will need to **remove** the **yourthemename.css** file and **yourthemename.uncompressed.css** file, from your theme's package.</p>

	<p class="alert-success">Since you left the option **"assign to website..."** checked, the CSS is already available for the website you have chosen. If you need to make the CSS available for other websites, with the **CSS stylesheet** you just created opened, go to the **Sites** tab and click on **Add sites**.</p>

**1# APPLY THE CSS STYLESHEETS TO YOUR WEBSITE**

Now let's apply the CSS stylesheet you created in step 1# Upload the CSS Stylesheets. 

1. Open the **Applications list**;
2. Click on **Configuration** and then click on **Sites**;
 	
 	![kentico9_rtd_installation-7.png](https://bitbucket.org/repo/yG87gj/images/2958331868-kentico9_rtd_installation-7.png)
 	
3. On the website you want to apply the CSS stylesheet, click on the **Edit** icon;
4. In the **General** tab, under the **Style sheets** section, in the **Site CSS stylesheet**, select the CSS stylesheet you created during step 1#;
	
	![kentico9_rtd_installation-6.png](https://bitbucket.org/repo/yG87gj/images/2950256444-kentico9_rtd_installation-6.png)
	
5. Click **Save**;

CSS Stylesheet added! ✅

-----
<a name="addthebindmenu"></a>
### 2# Add the BindMENU 

![BindMENU.gif](https://bitbucket.org/repo/yG87gj/images/1008315636-BindMENU.gif)

The BindMENU's are menus included in your theme's package.

To add a BindMENU you will need to add the code from different CSS files. You can find these files, inside your theme's package in your CSS folder. 

![kentico9_rtd_installation-8.png](https://bitbucket.org/repo/yG87gj/images/1611311408-kentico9_rtd_installation-8.png)

We will be installing the mobile-megamenu.css file, the mobile-select.css file, and the mobile-toggle.css file. 

<p class="alert-success">For themes with vertical menus, the menu might not appear if the page has no
content. Try adding some content before proceeding.</p>

1. Go back to your **Applications dashboard**;
2. Click on **CSS stylesheets**;
2. Click on **New CSS stylesheet**;
4. On the **General** and the **Code** section add:
	
	![kentico9_rtd_installation-10.png](https://bitbucket.org/repo/yG87gj/images/2732709258-kentico9_rtd_installation-10.png)
	
	1. On **Display name**, add the name **"mobile-megamenu"**.
	2. On **Code name**, add the name **"mobile-megamenu"**.
	3. On **Code**, paste the code from the **mobile-megamenu.css**. You can find this file by opening your theme's package and then the **css** folder.  

5. You can leave the option **"assign to website..."** checked; 
6. Click **Save**.  

<p class="alert-warning">Repeat the same steps for also the **mobile-select.css** file and the **mobile-toggle.css** file.</p>

<p class="alert-warning">Done adding the CSS? You will now need to remove the CSS files you just add, the mobile-megamenu.css file, the mobile-select.css file and the mobile-toggle.css, from the css folder, from inside your theme's package, since Kentico loads all css in that folder, eventually causing a few conflicts.</p>

BindMENU added! ✅

----------

<a name="addthemasterpageandpagetemplate"></a>
### 3# Add the Master Page(s) and Page Template

In your theme's package you will find different .ascx files, each related to a certain type of Master Page that is included in your theme. You will need to add these Master Pages in order to use them on your website. Note that you can install only the ones you want to use.

You will also find a .ascx file for the Page Template. First thing on the list is to create a folder/category on the Templates list for your theme's master pages and page template. Let's do this!


**1# CREATE A FOLDER/CATEGORY FOR THE THEME**

1. Open the **Applications list**;
2. Click on **Development** and then click on **Page Templates**;

	![kentico9_rtd_installation-11.png](https://bitbucket.org/repo/yG87gj/images/3137385635-kentico9_rtd_installation-11.png)
	
2. On the templates list, select the folder **All page templates**; 
3. Now click on the three dots menu, next to **New template**, and then click on **New category**;

	![kentico9_rtd_installation-12.png](https://bitbucket.org/repo/yG87gj/images/2208843341-kentico9_rtd_installation-12.png)
	
4. Let's create the folder/category: 
	
	1. On **Category display name**, add your theme's name.
	2. On **Category name**, add your theme's name.
		
		<p class="alert-success">You can leave **Category image path** empty.</p>
	
5. Click **Save**.


**2# ADD THE MASTER PAGE (s)**

<p class="alert-success">You can check the name of each of the master pages included in your theme by visiting your theme's live preview at bindtuning.com.</p>

<p class="alert-warning">Kentico only allows the use of one master page, so you will have to choose which .asx
you will want to use on your website. You can choose between different master pages, Home.ascx, Wide-sidebar.ascx - it depends on the theme you have downloaded.</p>

<p class="alert-warning">We will not be using the .ascx files that start with **C0***, since they are for adding the web part containers.</p>

1. On the templates list, select the newly created folder/category;
2. Now click on **New template**;
4. Let's create the master page. We will be adding the Wide master page, included in our example theme package: 

	![kentico9_rtd_installation-14.png](https://bitbucket.org/repo/yG87gj/images/1872081170-kentico9_rtd_installation-14.png)
	
	1. On **Template display name**, add the name **"yourthemename masterpagename"**. *Example: MyTheme Wide*
	2. On **Template code name**, add the name **"yourthemenamemasterpagename"**. Don't leave any spaces between your theme name and the name of the master page. *Example: MyThemeWide* 
	
5. Click **Save**;
6. On the templates list, select the template you just created.
7. On the **General** tab:

	![kentico9_rtd_installation-15.png](https://bitbucket.org/repo/yG87gj/images/2181783065-kentico9_rtd_installation-15.png)
	
	1. Make sure the **Template type** is set to **Portal page**;
	2. Check the **Master template** option; 
	
7. Click **Save**.
8. Now switch to the **Layout** tab;
9. Select the **Use custom layout** option;
10. Open the .ascx file of the master page you are creating with Notepad++. You can find it by opening your theme's package;

	![kentico9_rtd_installation-17.png](https://bitbucket.org/repo/yG87gj/images/1174404910-kentico9_rtd_installation-17.png)
	
11. Now copy the entire code and paste it on the code area;
12. Click **Save**.

Master Page added! ✅

**3# ADD THE PAGE TEMPLATE**

1. On the list, select again your theme's folder/category;
2. Now click on **New template**;
	
	![kentico9_rtd_installation-18.png](https://bitbucket.org/repo/yG87gj/images/145060597-kentico9_rtd_installation-18.png)
	
2. Select the folder **All page templates** on the list; 
3. Let's create the page template:

	1. On **Template display name**, add the name **"yourthemename Page Template"**. *Example: MyTheme Page Template*.
	2. On **Template code name**, add the name **"yourthemename.PageTemplate**. *Example: MyTheme.PageTemplate*.

4. Click **Save**;
5. With your newly created template selected on the list, on the **General** tab:
	
	![kentico9_rtd_installation-19.png](https://bitbucket.org/repo/yG87gj/images/3608220306-kentico9_rtd_installation-19.png)
	
	1. Make sure the **Template type** is set to **Portal page**;
	2. Check the **Master template** option; 

7. Click **Save**.
8. Now switch to the **Layout** tab;
9. Select the **Use custom layout** option;
10. Open the **PageTemplate.ascx** file with Notepad++. You can find it by opening your theme's package;
11. Now copy the entire code and paste it on the code area;
	
	![kentico9_rtd_installation-21.png](https://bitbucket.org/repo/yG87gj/images/1775091243-kentico9_rtd_installation-21.png)
	
12. Click **Save**.

Page Template added! ✅

----------

<a name="applythemasterpageandpagetemplate"></a>
### 4# Apply the Master Page and Page Template

Now that you're done adding the master pages and page template the only thing left to do is applying them to your website.

**1# APPLY THE MASTER PAGE**

1. Open the **Applications list**;
2. Click on **Pages**;

	![kentico9_rtd_installation-25.png](https://bitbucket.org/repo/yG87gj/images/2753728020-kentico9_rtd_installation-25.png)
	
3. With your site selected on the Pages list, click on **Properties** and click on **Template**;

	![kentico9_rtd_installation-24.png](https://bitbucket.org/repo/yG87gj/images/3561480127-kentico9_rtd_installation-24.png)
	
	<p class="alert-success">You can apply the master page to a single page. Just select the page from the page list instead.</p>

4. With the option **Use own page template** selected, click on **Select**;
5. On the templates list, click on the template you created with your theme name; 
6. Select the master page you added previously. *Example: MyTheme Wide*;
		
	![kentico9_rtd_installation-22.png](https://bitbucket.org/repo/yG87gj/images/4219796513-kentico9_rtd_installation-22.png)
	
7. Click **Select** and then click **Save**;

**2# APPLY THE PAGE TEMPLATE**

1. Open the **Applications list**;
2. Click on **Pages**;
3. With your site selected on the pages list, click on **Properties** and click on **Template**;
	
	<p class="alert-success">You can apply the master page to a single page. Just select the page from the page list instead.</p>

4. With the option **Use own page template** selected, click on **Select**;
5. On the templates list, click on the template you created with your theme name; 
6. Select the master page you added previously. *Example: MyTheme PageTemplate*;
	
	![kentico9_rtd_installation-22.png](https://bitbucket.org/repo/yG87gj/images/4219796513-kentico9_rtd_installation-22.png)
	
7. Click **Select** and then click **Save**;


-----

Installation done! With BindTuning themes, you also have access to demo content files, which can be directly imported into your Kentico website. If you want to go ahead and import the demo content, click on the link below.