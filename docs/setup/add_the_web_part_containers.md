Can you see .ascx files that start with **C0*** inside your theme's package? That means you can have web part containers in your website to make your content look even nicer. 

You can check the various containers included in your theme package, by visiting your theme's live preview at bindtuning.com.

<p class="alert-success">Repeat this process for each C0x.ascx file (C01.ascx, C02.ascx, C03.ascx, C04.ascx,...). </p>


1. Open the Application list;
2. Click on **Development** and click on **Web Part Containers**;
	
	![kentico9_rtd_containers-1.png](https://bitbucket.org/repo/yG87gj/images/3206025669-kentico9_rtd_containers-1.png)
	
3. Click on **New container**;
4. Apply the contents of the C0x.ascx file:

	- **Text before the web part**: Copy the all content of the file till /* END TEXT BEFORE
WEBPART */ and paste it here.
	- **Text after web part:** Copy the all content of the file from /* START TEXT AFTER
WEBPART */ and paste it here.

