Starting in release X.X.X.2, BindTuning themes are packaged including demo content which can be directly imported into your Kentico website.

<p class="alert-warning"> Your BindTuning theme must be installed and applied to the site before uploading the demo content.
</p> 

<a name="downloadthe.zipfile"></a>
### 1# Download the .zip file 

1. Access your account at <a href="http://bindtuning.com" target="_blank">bindtuning.com</a>; 
2. Open the **My Downloads** tab;
3. Mouse hover your theme and click on **View theme**;
4. Under documentation you will find the demo content .zip file.

![kentico9_rtd_uploadthedemocontent.png](https://bitbucket.org/repo/yG87gj/images/31940201-kentico9_rtd_uploadthedemocontent.png)

----

<a name="unzipthefile"></a>
### 2# Unzip the file

Before uploading the demo content into your website you will need to unzip the file. The file name is **theme.Kentico9.DemoContent.zip**.

After unzipping the file, open the demo content file(.txt) that you want to add and copy all the content.

![kentico9_rtd_uploadthedemocontent-2.png](https://bitbucket.org/repo/yG87gj/images/1169111658-kentico9_rtd_uploadthedemocontent-2.png)


----

<a name="addthedemocontent"></a>
### 3# Add the demo content 
If you want to copy the content to the Main Content Zone, make sure to follow these steps:

1. Log in to your Kentico website as an Admin;
2. Open your **Applications list**, click on **Development** and then on **Page Templates**;

	![kentico9_rtd_uploadthedemocontent-5.png](https://bitbucket.org/repo/yG87gj/images/4132374167-kentico9_rtd_uploadthedemocontent-5.png)
	
2. On the **Templates list**, click on the **Ad-hoc** folder;
3. On the **Display name**, select the rule **Contains** and on the box next to it type the page of your website where you want to add the demo content. Now click on **Search**;	

	![kentico9_rtd_uploadthedemocontent-4.png](https://bitbucket.org/repo/yG87gj/images/3868011047-kentico9_rtd_uploadthedemocontent-4.png)
	
4. Click on the **Edit** icon; 
5. Switch to the **Web Parts** tab;
	
6. With Notepad++, open the .xml file of the master you are using on the page and copy the entire code. The name should be something like **DemoContentFor_nameofthemaster_Page.xml**. You will find this file inside your theme's demo content .zip file;
	
	![kentico9_rtd_uploadthedemocontent-6.png](https://bitbucket.org/repo/yG87gj/images/3070439652-kentico9_rtd_uploadthedemocontent-6.png)
	
7. Paste the code in the **Web Part configuration** box. The code should be placed between the <page> tags;
	
	![kentico9_rtd_uploadthedemocontent-7.png](https://bitbucket.org/repo/yG87gj/images/2641240491-kentico9_rtd_uploadthedemocontent-7.png)
	
8. Click **Save**.


Demo content added! ✅ 
